﻿using UnityEngine;
using System.Collections;

public class CoroutineTestController : MonoBehaviour 
{
    [SerializeField]
    private float delayTime;
    [SerializeField]
    private bool isRandom;
    [SerializeField]
    private float maxRandomDelayTime;
    [SerializeField]
    private float minRandomDealyTime;
    private IEnumerator coroutinePlaySoundRandom;
    private IEnumerator coroutinePlaySound;

    private void OnEnable()
    {
        Debug.Log("OnEnable");
        Init();
    }

    private void Init()
    {
        if (isRandom)
        {
            coroutinePlaySoundRandom = PlaySoundCoroutineRandom();
            StartCoroutine(coroutinePlaySoundRandom);
        }
        else
        {
            coroutinePlaySound = PlaySoundCoroutine();
            StartCoroutine(coroutinePlaySound);
        }
    }

    private IEnumerator PlaySoundCoroutine()
    {
        Debug.Log("PlaySoundCoroutine");
        while (true)
        {
            yield return new WaitForSeconds(delayTime);
            Debug.Log("PlaySoundCoroutine - Run");
        }
    }

    private IEnumerator PlaySoundCoroutineRandom()
    {
        Debug.Log("PlaySoundCoroutineRandom");
        while (true)
        {
            float randomDelayTime = Random.Range(minRandomDealyTime, maxRandomDelayTime);
            yield return new WaitForSeconds(randomDelayTime);
            Debug.Log("PlaySoundCoroutineRandom - Run");
        }
    }

    private void OnDisable()
    {
        Debug.Log("OnDisable");
        if (isRandom)
        {
            StopCoroutine(coroutinePlaySoundRandom);
        }
        else
        {
            StopCoroutine(coroutinePlaySound);
        }
    }
}
