﻿using UnityEngine;
using System.Collections;
using System;

public class PlayerMove : MonoBehaviour
{
    public Transform cameraPos;

    [SerializeField] float moveSpeed = 10.0f;
    [SerializeField] float rotateSpeed = 10.0f;
    [SerializeField] float jumpPower = 500f;
    bool isJumping = false;
    Transform playerTransform;
    Rigidbody playerRigidbody;
    Camera mainCamera;

    float xAxis = 0f;
    float yAxis = 0f;
    
    void Start()
    {
        playerTransform = transform;
        mainCamera = Camera.main;
        playerRigidbody = GetComponent<Rigidbody>();
    }
    
    //void Update()
    //{
    //    //MoveUpdateByInpuKey();
    //    //JumpUpdate();
    //}

    void FixedUpdate()
    {
        MoveUpdateByPhysics();
        JumpUpdateByPhysics();
    }
    
    void LateUpdate()
    {
        CameraUpdate();
    }

    void OnCollisionEnter(Collision col)
    {
        if(col.collider.name == "Floor")
        {
            isJumping = false;
        }
    }

    private void MoveUpdateByPhysics()
    {
        if (Input.GetKey(KeyCode.W))
        {
            playerRigidbody.MovePosition(playerTransform.position + playerTransform.forward * moveSpeed * Time.fixedDeltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            playerRigidbody.MovePosition(playerTransform.position + -playerTransform.forward * moveSpeed * Time.fixedDeltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            playerRigidbody.MovePosition(playerTransform.position + -playerTransform.right * moveSpeed * Time.fixedDeltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            playerRigidbody.MovePosition(playerTransform.position + playerTransform.right * moveSpeed * Time.fixedDeltaTime);
        }
    }

    private void JumpUpdateByPhysics()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isJumping == true) return;
            isJumping = true;
            playerRigidbody.AddForce(Vector3.up * jumpPower, ForceMode.Force);
        }
    }

    private void MoveUpdateByInpuKey()
    {
        if (Input.GetKey(KeyCode.W))
        {
            playerTransform.Translate(Vector3.forward * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.S))
        {
            playerTransform.Translate(Vector3.back * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.A))
        {
            playerTransform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
        }
        if (Input.GetKey(KeyCode.D))
        {
            playerTransform.Translate(Vector3.right * moveSpeed * Time.deltaTime);
        }
    }

    private void JumpUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (isJumping == true) return;

            isJumping = true;
            jumpPower = 10f;
        }

        playerTransform.Translate(Vector3.up * jumpPower * Time.deltaTime);
        jumpPower -= 20 * Time.deltaTime;

        if (playerTransform.position.y < 1.01f)
        {
            playerTransform.position = new Vector3(playerTransform.position.x, 1f, playerTransform.position.z);
            jumpPower = 0;
            isJumping = false;
        }
    }

    private void CameraUpdate()
    {
        xAxis = Input.GetAxis("Mouse X");
        yAxis = Input.GetAxis("Mouse Y");

        if (xAxis != 0)
        {
            playerTransform.Rotate(Vector3.up, xAxis * rotateSpeed * Time.deltaTime);
        }

        if (yAxis != 0)
        {
            cameraPos.Rotate(Vector3.left, yAxis * rotateSpeed * Time.deltaTime);
        }
    }

}
