﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ComponentSample : MonoBehaviour
{
    public List<TestComponent> TestComponents;

	void Update () 
    {
		// TestComponents 리스트에 있는 아이템 갯수 확인
        Debug.Log("test components count : " + TestComponents.Count);

		// TestComponents 안에 있는 아이템에 접근해서 수정 가능한지 확인.
		// TestComponent를 on, off해도 계속 접근할 수 있음 확인 가능
	    foreach (var testComponent in TestComponents)
	    {
	        Debug.Log("value : " + testComponent.Val);
            testComponent.Val = Random.Range(0, 10);
	    }	
	}
}
