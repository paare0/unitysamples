﻿using UnityEngine;
using System.Collections.Generic;

public class PMRadarBoardController : MonoBehaviour
{
    public RectTransform RadarBoardRectTransform;
    public Transform PlayerTransform;
    public GameObject RadarDot;
    public List<PMRadarDot> RadarDots;
    
    public void AddDotOnBoard(Transform enemyTransform)
    {
        GameObject go = Instantiate(RadarDot, Vector3.zero, Quaternion.identity) as GameObject;
        go.transform.parent = RadarBoardRectTransform;
        go.transform.localPosition = Vector3.zero;

        PMRadarDot dot = go.GetComponent<PMRadarDot>();
        dot.EnemyTransform = enemyTransform;
        dot.PlayerTransform = PlayerTransform;
        RadarDots.Add(dot);        
    }

    public void RemoveDotOnBoard(Transform enemyTransform)
    {
        for (int i = 0; i < RadarDots.Count; i++)
        {
            if (RadarDots[i].EnemyTransform.Equals(enemyTransform))
            {
                Destroy(RadarDots[i].gameObject);
                RadarDots.Remove(RadarDots[i]);
                break;
            }
        }
    }


}