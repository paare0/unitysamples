﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PMRadarDot : MonoBehaviour
{
    public Transform EnemyTransform;
    public Transform PlayerTransform;
    public RectTransform ImageRectTransform;

    private void Start()
    {
        ImageRectTransform = GetComponent<Image>().rectTransform;
    }

    private void Update()
    {
        float distance = Vector3.Distance(PlayerTransform.position, EnemyTransform.position);
        Vector3 targetDir = EnemyTransform.position - PlayerTransform.position;
        float angle = Angle360(PlayerTransform.forward, targetDir);
        Quaternion rot = Quaternion.Euler(0, angle, 0);
        Vector3 postion = rot * Vector3.forward * distance * 30;
        transform.localPosition = new Vector3(postion.x, postion.z);
    }

    public float Angle360(Vector3 fwd, Vector3 targetDir)
    {
        float angle = Vector3.Angle(fwd, targetDir);

        if (AngleDir(fwd, targetDir, Vector3.up) == -1)
        {
            angle = 360.0f - angle;
            if (angle > 359.9999f)
                angle -= 360.0f;
            return angle;
        }
        return angle;
    }
    
    // Unity Vector3.Angle()은 180까지만 줘서 보정이 필요함.
    public int AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up)
    {
        Vector3 perp = Vector3.Cross(fwd, targetDir);
        float dir = Vector3.Dot(perp, up);

        if (dir > 0.0)
            return 1;
        if (dir < 0.0)
            return -1;
        return 0;
    }
}
