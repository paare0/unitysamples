﻿using UnityEngine;
using System.Collections;

public class CoroutineSample : MonoBehaviour
{
    private bool isRun = true;
	IEnumerator Start ()
	{
	    StartCoroutine("TestCoroutine");

        yield return new WaitForSeconds(3);

	    isRun = false;
	}

    IEnumerator TestCoroutine()
    {
        while (isRun)
        {
            Debug.Log("11111111111111111111111111");
        }
        yield return new WaitForEndOfFrame();

        StopCoroutine("TestCoroutine");

        Debug.Log("2222222222222222222222");
    }
}
