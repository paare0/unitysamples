﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SceneManagerTest : MonoBehaviour
{
    public List<Scene> Scenes = new List<Scene>();
	// Use this for initialization
	IEnumerator Start ()
    {
	    for (int i = 0; i < 4; i++)
	    {
	        SceneManager.LoadSceneAsync(string.Format("TestScene {0}", i), LoadSceneMode.Additive);
	    }

        yield return new WaitForSeconds(3f);

        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            var _scene = SceneManager.GetSceneAt(i);
            if (_scene.name.Contains("TestScene"))
            {
                Scenes.Add(_scene);
            }
        }

        Scenes.ForEach(s => Debug.Log(s.name));

        //Invoke("RemoveScenes", 1f);
	}


    private void RemoveScenes()
    {
        Debug.Log("Scnene count : "+SceneManager.sceneCount);
        var scenes = new List<Scene>();
        for (int i = 0; i < SceneManager.sceneCount; i++)
        {
            var scene = SceneManager.GetSceneAt(i);
            Debug.Log(i + " " + scene.name);
            if (scene.name.Equals("TestScene"))
            {
                scenes.Add(scene);
            }
        }

        Debug.Log("Scnenes count : " + scenes.Count);
        foreach (var scene in scenes)
        {
            Debug.Log("scene name : " + scene.name);
            SceneManager.UnloadScene(scene.name);
        }
    }

	// Update is called once per frame
	void Update () {
	
	}
}
