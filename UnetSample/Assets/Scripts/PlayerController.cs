﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.Networking;
using UnityEngine.UI;

public class PlayerController : NetworkBehaviour
{
    [SyncVar(hook = "OnValueChanged")]
    //[SyncVar]
    public int _playerNo = 0;

    [SerializeField] private Text playerName;
    public float Speed = 1;

    public override void OnStartClient()
    {
        base.OnStartClient();
        Debug.Log("OnStartClient");
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        Debug.Log("OnStartLocalPlayer");
        if (isLocalPlayer)
        {
            string readStr = File.ReadAllText(Application.dataPath + "/sample_data.txt");
            _playerNo = int.Parse(readStr);

            CmdSetPlayerNo(_playerNo);
            CmdSetPlayerName();
        }
    }

    private void Start()
    {
        Debug.Log("start() - player no : " + _playerNo);
        if (!isLocalPlayer)
        {
            GetComponentInChildren<Camera>().gameObject.SetActive(false);
        }

        playerName.text = "Player " + _playerNo;
    }

    private void Update()
    {
        if (isLocalPlayer)
        {
            //if (Input.GetKey(KeyCode.A))
            //{
            //    transform.Translate(-Speed * Time.deltaTime, 0, 0, transform);
            //}

            //if (Input.GetKey(KeyCode.D))
            //{
            //    transform.Translate(Speed * Time.deltaTime, 0, 0, transform);
            //}

            //if (Input.GetKey(KeyCode.W))
            //{
            //    transform.Translate(0, 0, -Speed * Time.deltaTime, transform);
            //}

            //if (Input.GetKey(KeyCode.S))
            //{
            //    transform.Translate(0, 0, Speed * Time.deltaTime, transform);
            //}
            transform.Translate(0, 0, Speed * Time.deltaTime, transform);
        }
    }

    private void OnValueChanged(int changedNo)
    {
        Debug.Log("OnValueChanged to : " + changedNo);
    }
    

    [Command]
    private void CmdSetPlayerNo(int no)
    {
        _playerNo = no;
    }

    [Command]
    private void CmdSetPlayerName()
    {
        if (isServer)
        {
            RpcSetPlayerName();
        }
        else
        {
            playerName.text = "Player " + _playerNo;
        }
    }

    [ClientRpc]
    private void RpcSetPlayerName()
    {
        playerName.text = "Player " + _playerNo;
    }
    
}
